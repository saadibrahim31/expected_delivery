Expected delviery Calculator
====
## Run the project
### Setup
- `./install.sh`
###OR run below commands
```
docker-compose -f ./docker/docker-compose.yml up -d --build
```

```
docker-compose -f ./docker/docker-compose.yml exec phpproductoriginal php composer.phar install
```


##Result
####carrier data:

```
http://localhost/api/v1/carriers
```

```json5
[
  {
    "id": 1,
    "carrierName": "DHL",
    "deliveryDays": 2
  },
  {
    "id": 2,
    "carrierName": "GLS",
    "deliveryDays": 2
  },
  {
    "id": 3,
    "carrierName": "DPD",
    "deliveryDays": 4
  },
  {
    "id": 4,
    "carrierName": "DHL",
    "deliveryDays": 3
  }
]
```


### to call api from your browser.

####api documentation
```
http://localhost/api/doc
```
```
http://localhost/api/v1/expected-delivery/2020-05-01%2011:21:38/2 method={"GET"}
```

http code: 200
```json5
{"actualDate":"2020-05-01T11:21:38+00:00","expectedDate":"2020-05-06"}
```

```

http://localhost/api/v1/expected-delivery/2020-04-13%2011:21:38/2 method={"GET"}
```

http code: 200
```json5
{"actualDate":"2020-04-13T11:21:38+00:00","expectedDate":"2020-04-16"}
```


#####different carrier
```
http://localhost/api/v1/expected-delivery/2020-04-13%2011:21:38/3 method={"GET"}
```

http code: 200
```json5
{"actualDate":"2020-04-13T11:21:38+00:00","expectedDate":"2020-04-18"}
```

```
http://localhost/api/v1/expected-delivery/2021-04-01%2011:21:38/2 method={"GET"}
```

http code: 200
```json5
{"actualDate":"2021-04-01T11:21:38+00:00","expectedDate":"2021-04-08"}
```



```
http://localhost/api/v1/expected-delivery/2021-04-01%201033:21:38/2 method={"GET"}
```

http code: 400
```json5
{"message":"","data":{"errors":{"actualDate":"The value \u00222021-04-01 1033:21:38\u0022 is not a valid Y-m-d H:i:s."},"status":400,"type":"validation_error","title":"There was a validation error"}}
```

Request
```
http://localhost/api/v1/expected-delivery/2020-02-14%2009:21:38/5 method={"GET"}
```

response     status code: 404
```json5
{"message":"carrier with id \u00275\u0027 is not found!","data":[]}
```

Request:
```
http://localhost/api/v2/expected-delivery/2021-04-01%2010:21:38/5 method={"GET"}
```
code status code: 404

```json5
{"message":"No route found for \u0022GET \/api\/v2\/expected-delivery\/2021-04-01%2010:21:38\/5\u0022","data":[]}
```


###if you miss data in database please call.

```
 php bin/console doctrine:fixtures:load
```

###Assumptions

1. Not to consider all holidays from previous years and future in our database, I considered 4 months from "actual datetime" as start.
  why i did that cause of holidays database could be hug after some years and usually packet is not taking more than 4 months to be delivered.
  this condition can be configurable from if needed(there is no requirements about that).
2. You can add interface for a backoffice to add the holdiays, and carriers, repos is ready for CRUD operations.
3. cuttof time shoud be 12 pm but i set it 11 am as in your expected data it shows you use cutoff time when clock pass 11 am., after that day is count +1 days. 
(please correct me if i'm wrong, i already sent a question about that to HR but no answer)
