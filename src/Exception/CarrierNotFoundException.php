<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class CarrierNotFoundException
 * @package App\Exception
 */
class CarrierNotFoundException extends HttpException
{
    protected $message = "Carrier is not found";
}