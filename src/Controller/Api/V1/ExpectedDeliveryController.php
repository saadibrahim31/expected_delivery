<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Exception\ApiProblem;
use App\Exception\ApiProblemException;
use App\Repository\CarrierRepository;
use App\Service\DeliveryService;
use App\Validator\InputValidator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ExpectedDeliveryController
 * @package App\Controller
 */
class ExpectedDeliveryController extends AbstractFOSRestController
{
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * @var DeliveryService
     */
    private DeliveryService $deliveryService;
    /**
     * @var CarrierRepository
     */
    private CarrierRepository $carrierRepository;

    /**
     * ReviewsController constructor.
     * @param ValidatorInterface $validator
     * @param DeliveryService $deliveryService
     */
    public function __construct(validatorInterface $validator, DeliveryService $deliveryService, CarrierRepository $carrierRepository)
    {
        $this->validator = $validator;
        $this->deliveryService = $deliveryService;
        $this->carrierRepository = $carrierRepository;
    }

    /**
     *
     * Get expected delivery based on actual date and carrier
     *
     * @Rest\Get("/api/v1/expected-delivery/{actualDate}/{carrierId}")
     * @OA\Response(
     *     response=200,
     *     description=" Ok "
     * ),
     * @OA\Response(
     *     response=400,
     *     description="bad request"
     * ),
     * @OA\Response(
     *     response=404,
     *     description="Not found"
     * )
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getExpectedDelivery(Request $request): Response
    {
        $errors = InputValidator::validateExpectedDelivery($request, $this->validator);
        if (count($errors) > 0) {
            $this->throwApiProblemValidationException($errors);
        }

        $actualDate = new \DateTimeImmutable($request->get('actualDate'));
        $expectedDeliveryResponse = $this->deliveryService->calculateExpectedDate($actualDate, (int)$request->get('carrierId'));

        return $this->handleView($this->view($expectedDeliveryResponse, 200));
    }
    /**
     * List all carriers.
     *
     *
     * @Rest\Get("/api/v1/carriers")
     * @OA\Response(
     *     response=200,
     *     description="Returns the Carriers"
     * )
     * @OA\Tag(name="Carrier")
     */
    public function getCarriers()
    {
        $carriers = $this->carrierRepository->findAll();
        return $this->handleView($this->view($carriers, 200));
    }

    /**
     * @param array $errors
     * @throws \Exception
     */
    private function throwApiProblemValidationException(array $errors)
    {
        $apiProblem = new ApiProblem(
            400,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        $apiProblem->set('errors', $errors);

        throw new ApiProblemException($apiProblem);
    }
}