<?php

declare(strict_types=1);
namespace App\EventListener;

use App\Exception\ApiProblemException;
use App\Http\ApiResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $request   = $event->getRequest();
        if (false !== strpos((string)$request->get('_route'), 'app_api_')
            || $request->get('media_type' )=== 'application/json'
        ) {
            $response = $this->createApiResponse($exception);
            $event->setResponse($response);
        }
    }

    /**
     * Creates the ApiResponse from any Exception
     *
     * @param \Throwable $exception
     *
     * @return ApiResponse
     */
    private function createApiResponse(\Throwable $exception)
    {
        $statusCode = $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        $message = $exception->getMessage();
        $errors = $data = [];
        if ($exception instanceof ApiProblemException) {
            $apiProblem = $exception->getApiProblem();
            $statusCode = $apiProblem->getStatusCode();
            $data = $apiProblem->toArray();
            $message = '';
        }

        return new ApiResponse($message, $data, $errors, $statusCode);
    }
}