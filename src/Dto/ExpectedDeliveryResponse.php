<?php

declare(strict_types=1);

namespace App\Dto;

/**
 * Class ExpectedDeliveryResponse
 * @package App\Dto
 */
class ExpectedDeliveryResponse
{
    /**
     * @var string
     */
    private string $expectedDate;

    /**
     * @var \DateTimeImmutable
     */
    private \DateTimeImmutable $actualDate;

    /**
     * ExpectedDeliveryResponse constructor.
     * @param \DateTimeImmutable $actualDate
     * @param string $expectedDate
     */
    public function __construct(\DateTimeImmutable $actualDate, string $expectedDate)
    {
        $this->expectedDate = $expectedDate;
        $this->actualDate = $actualDate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getActualDate(): \DateTimeImmutable
    {
        return $this->actualDate;
    }

    /**
     * @return string
     */
    public function getExpectedDate(): string
    {
        return $this->expectedDate;
    }
}