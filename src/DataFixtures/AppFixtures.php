<?php

namespace App\DataFixtures;

use App\Entity\Carrier;
use App\Entity\Holiday;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    private array $carriers = [
        [
            'carrierName' => 'DHL',
            'deliveryDays' => 2
        ],
        [
            'carrierName' => 'GLS',
            'deliveryDays' => 2
        ],
        [
            'carrierName' => 'DPD',
            'deliveryDays' => 4
        ],
        [
            'carrierName' => 'DHL',
            'deliveryDays' => 3
        ]
    ];

    private array $holidays;

    /**
     * AppFixtures constructor.
     */
    public function __construct()
    {
        $this->holidays = [
            ['date' => new \DateTimeImmutable('2021-01-01'), 'name' => 'New Year\'s Day'], ['date' => new \DateTimeImmutable('2021-03-08'), 'name' => 'International Women\'s Day',],
            ['date' => new \DateTimeImmutable('2021-04-02'), 'name' => 'Good Friday'], ['date' => new \DateTimeImmutable('2021-04-05'), 'name' => 'Easter Monday',],
            ['date' => new \DateTimeImmutable('2021-05-01'), 'name' => 'Labour Day'], ['date' => new \DateTimeImmutable('2021-05-13'), 'name' => 'Ascension Day',],
            ['date' => new \DateTimeImmutable('2021-05-24'), 'name' => 'whit Monday'], ['date' => new \DateTimeImmutable('2021-10-03'), 'name' => 'Day Of German unity',],
            ['date' => new \DateTimeImmutable('2021-12-25'), 'name' => 'Christmas Day'], ['date' => new \DateTimeImmutable('2021-12-26'), 'name' => 'Boxing Day',],

            ['date' => new \DateTimeImmutable('2020-01-01'), 'name' => 'New Year\'s Day'], ['date' => new \DateTimeImmutable('2020-03-08'), 'name' => 'International Women\'s Day',],
            ['date' => new \DateTimeImmutable('2020-04-10'), 'name' => 'Good Friday'], ['date' => new \DateTimeImmutable('2020-04-13'), 'name' => 'Easter Monday',],
            ['date' => new \DateTimeImmutable('2020-05-01'), 'name' => 'Labour Day'], ['date' => new \DateTimeImmutable('2020-05-21'), 'name' => 'Ascension Day',],
            ['date' => new \DateTimeImmutable('2020-05-08'), 'name' => '5th Anniversary of the End of WWII'],
            ['date' => new \DateTimeImmutable('2020-06-01'), 'name' => 'whit Monday'], ['date' => new \DateTimeImmutable('2020-10-03'), 'name' => 'Day Of German unity',],
            ['date' => new \DateTimeImmutable('2020-12-25'), 'name' => 'Christmas Day'], ['date' => new \DateTimeImmutable('2020-12-26'), 'name' => '2nd Day of Christmas',],
        ];
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadCarriers($manager);
        $this->loadHolidays($manager);
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadCarriers(ObjectManager $manager)
    {
        foreach ($this->carriers as $carrier) {
            $carrierEntity = new Carrier(null, $carrier['carrierName'], $carrier['deliveryDays']);
            $manager->persist($carrierEntity);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadHolidays(ObjectManager $manager)
    {
        foreach ($this->holidays as $holiday) {
            $holidayEntity = new Holiday(null, $holiday['date'], $holiday['name']);
            $manager->persist($holidayEntity);
        }
        $manager->flush();
    }
}
