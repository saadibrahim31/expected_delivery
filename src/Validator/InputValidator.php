<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ValidateInput
 * @package App\Validator
 */
class InputValidator
{
    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return array
     * @throws \Exception
     */
    public static function validateExpectedDelivery(Request $request, validatorInterface $validator): array
    {
        $carrierId = $request->get('carrierId');
        $actualDate = $request->get('actualDate');
        $inputs = ['carrierId' => (int)$carrierId, 'actualDate' => $actualDate];

        $constraints = new Assert\Collection([
            'carrierId' => [
                new Assert\Type("integer", "The value {{ value }} is not a valid {{ type }}.")
            ], 'actualDate' => [
                new Assert\DateTime("Y-m-d H:i:s", "The value {{ value }} is not a valid Y-m-d H:i:s."),
                new Assert\NotBlank
            ]
        ]);

        $violations = $validator->validate($inputs, $constraints);
        $errorMessages = [];
        if (count($violations) > 0) {
            $accessor = PropertyAccess::createPropertyAccessor();
            foreach ($violations as $violation) {
                $accessor->setValue($errorMessages,
                    $violation->getPropertyPath(),
                    $violation->getMessage());
            }
        }

        return $errorMessages;
    }
}