<?php

namespace App\Entity;

use App\Repository\CarrierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarrierRepository::class)
 */
class Carrier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private string $carrierName;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $deliveryDays;

    /**
     * Carrier constructor.
     * @param int|null $id
     * @param $carrierName
     * @param int $deliveryDays
     */
    public function __construct(?int $id, $carrierName, int $deliveryDays)
    {
        $this->id = $id;
        $this->carrierName = $carrierName;
        $this->deliveryDays = $deliveryDays;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }

    public function getDeliveryDays(): ?int
    {
        return $this->deliveryDays;
    }
}
