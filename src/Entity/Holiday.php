<?php

namespace App\Entity;

use App\Repository\HolidayRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HolidayRepository::class)
 */
class Holiday
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $date;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private string $name;

    /**
     * Holiday constructor.
     *
     * @param int $id
     * @param \DateTimeImmutable $date
     * @param string $name
     */
    public function __construct(?int $id, \DateTimeImmutable $date, string $name)
    {
        $this->id = $id;
        $this->date = $date;
        $this->name = $name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}
