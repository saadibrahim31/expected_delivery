<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\ExpectedDeliveryResponse;
use App\Exception\CarrierNotFoundException;
use App\Repository\CarrierRepository;
use App\Repository\HolidayRepository;

/**
 * Class ExpectedDeliveryService
 * @package App
 */
class DeliveryService
{
    private const SATURDAY = 6;
    private const SUNDAY = 7;

    /**
     * @var CarrierRepository
     */
    private CarrierRepository $carrierRepository;

    /**
     * @var HolidayRepository
     */
    private HolidayRepository $holidayRepository;

    /**
     * ExpectedDeliveryService constructor.
     *
     * @param CarrierRepository $carrierRepository
     * @param HolidayRepository $holidayRepository
     */
    public function __construct(CarrierRepository $carrierRepository, HolidayRepository $holidayRepository)
    {
        $this->carrierRepository = $carrierRepository;
        $this->holidayRepository = $holidayRepository;
    }

    /**
     * @param \DateTime $actualDate
     * @param int $carrierId
     * @return ExpectedDeliveryResponse
     * @throws CarrierNotFoundException
     */
    public function calculateExpectedDate(\DateTimeImmutable $actualDate, int $carrierId): ExpectedDeliveryResponse
    {
        $carrierEntity = $this->carrierRepository->find($carrierId);
        if (null === $carrierEntity) {
            throw  new CarrierNotFoundException(404, sprintf("carrier with id '%s' is not found!", $carrierId));
        }

        $holidays = $this->holidayRepository->findDateWithinRange($actualDate, $actualDate->modify("+ 4 months"));// get holidays for next four months only
        $expectedDate =  $this->addBusinessDays($carrierEntity->getDeliveryDays(), $actualDate, $holidays);

        return new ExpectedDeliveryResponse($actualDate, $expectedDate->format("Y-m-d"));
    }

    /**
     * @param int $howManyDays
     * @param \DateTimeImmutable $actualDate
     * @param array $holidays
     * @return \DateTimeImmutable
     */
    private function addBusinessDays(int $howManyDays, \DateTimeImmutable $actualDate, array $holidays): \DateTimeImmutable
    {
        $i = 0;
        $newDate = clone $actualDate;
        while ($i < $howManyDays) {
            $newDate = $newDate->modify("+1 day");
            if ($this->isBusinessDay($newDate, $holidays)) {
                $i++;
            }
        }
        if ($this->isTimePassedCuttOffHour($newDate)) {
            $newDate = $newDate->modify("+1 day");
        }

        return $newDate;
    }

    /**
     * @param \DateTimeImmutable $newDate
     * @return bool
     */
    private function isTimePassedCuttOffHour(\DateTimeImmutable $newDate): bool
    {
        return (int)$newDate->format('G') >= 11;
    }

    /**
     * @param \DateTimeImmutable $actualDate
     * @param array $holidays
     * @return bool
     */
    private function isBusinessDay(\DateTimeImmutable $actualDate, array $holidays)
    {
        $weekendDays = [self::SUNDAY, self::SATURDAY];
        if (in_array((int)$actualDate->format('N'), $weekendDays)) {
            return false; //Date is a nonBusinessDay.
        }

        foreach ($holidays as $day) {
            if ($actualDate->format('Y-m-d') == $day->format('Y-m-d')) {
                return false; //Date is a holiday.
            }
        }

        return true; //Date is a business day.
    }
}