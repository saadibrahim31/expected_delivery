#!/bin/bash

docker-compose -f docker/docker-compose.yml exec phpproduct rm -rf vendor
docker-compose -f docker/docker-compose.yml down -v
