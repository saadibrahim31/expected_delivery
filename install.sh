#!/bin/bash

docker-compose -f ./docker/docker-compose.yml up -d --build --remove-orphans
docker-compose -f ./docker/docker-compose.yml exec phpproduct composer install
